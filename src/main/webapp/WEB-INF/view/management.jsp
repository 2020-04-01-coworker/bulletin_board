<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>ユーザー一覧</title>
<link href="<c:url value="/resources/css/management.css" />"	rel="stylesheet">
</head>
<body>
	<div class="wrapper">
		<div class="header-wrap">
			<h1>
				<a href="http://localhost:8080/BulletinboardBySpring/"><img src="<c:url value="/resources/img/logo.png" />" alt="ALH株式会社"  width="206" height="80"></a>
			</h1>
			<br />

			<ul id="nav">
				<li><a href="http://localhost:8080/BulletinboardBySpring/">ホーム</a></li>
				<li><a href="http://localhost:8080/BulletinboardBySpring/signup">ユーザー新規登録</a></li>
			</ul>
		</div>

		<div id="main">

			<c:if test="${not empty errorMessage}">
				<div class="errorMessages">
					<span>${errorMessage}</span>
					<br />
				</div>
			</c:if>
			<div class="user-list-wrapper">
				<c:forEach items="${users}" var="user">
					<div class="userlist">
						<p>
							アカウント：
							<c:out value="${user.account}"></c:out>
						</p>
						<p>
							ユーザー名：
							<c:out value="${user.userName}"></c:out>
						</p>
						<p>
							支社名：
							<c:out value="${user.branchName}"></c:out>
						</p>
						<p>
							部署名：
							<c:out value="${user.departmentName}"></c:out>
						</p>

						<c:if test="${user.isStopped == 0}">
							<p>
								状態：
								<c:out value="稼働中" />
							</p>
						</c:if>

						<c:if test="${user.isStopped != 0}">
							<p>
								状態：
								<c:out value="停止中" />
							</p>
						</c:if>

						<div class="edit-stop-button-wrapper">
							<form action="${pageContext.request.contextPath}/setting"
								method="GET">
								<input type="hidden" name="userId" value="${user.id}">
								<input type="submit" value="編集" class="button">
							</form>

							<c:if test="${user.id != loginUser.id }">
								<c:if test="${user.isStopped == 0 }">
									<form:form modelAttribute="userForm"
										action="${pageContext.request.contextPath}/management/update">
										<form:input path="id" type="hidden" value="${user.id}" />
										<form:input path="isStopped" type="hidden" value="1" />
										<input type="submit" value="停止" onclick='return confirm("アカウントの運用を一時停止します");' class="button">
									</form:form>
								</c:if>

								<c:if test="${user.isStopped == 1 }">
									<form:form modelAttribute="userForm"
										action="${pageContext.request.contextPath}/management/update">
										<form:input path="id" type="hidden" value="${user.id}" />
										<form:input path="isStopped" type="hidden" value="0" />
										<input type="submit" value="再開" onclick='return confirm("アカウントの運用を再開します");' class="button">
									</form:form>
								</c:if>
							</c:if>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	<div class="push"></div>
	</div>
	<div id="footer">
		&copy; 2020 Bulletin Board Team
	</div>
	<div id="page_top">
		<a href="#">PAGE TOP</a>
	</div>
</body>
</html>