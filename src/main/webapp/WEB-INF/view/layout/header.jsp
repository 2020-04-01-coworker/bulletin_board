<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="header-wrap">
		<h1><a href="http://localhost:8080/BulletinboardBySpring/"><img src="<c:url value="/resources/img/logo.png" />" alt="ALH株式会社" width="206" height="80"></a></h1><br />

		<ul id="nav">
			<li><a href="http://localhost:8080/BulletinboardBySpring/">ホーム</a></li>
			<li><a href="http://localhost:8080/BulletinboardBySpring/message">ユーザー新規投稿</a></li>
			<c:if test="${loginUser.departmentId == 1}">
				<li><a href="http://localhost:8080/BulletinboardBySpring/management">ユーザー管理</a></li>
			</c:if>
			<li><a href="http://localhost:8080/BulletinboardBySpring/logout">ログアウト</a></li>
		</ul>
</div>