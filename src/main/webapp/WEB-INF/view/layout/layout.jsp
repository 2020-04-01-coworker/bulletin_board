<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/error/error.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>${param.title}</title>

		<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>
		<!-- animsition.css -->
		<link rel="stylesheet" href="/resources/ccs/animsition.css">
		<!-- animsition.js -->
		<script src="/resources/js/animsition.js"></script>

	</head>
	<body>
		<jsp:include page="header.jsp"/>
			<div class="main-contents">
				${param.content}
			</div>
		<jsp:include page="footer.jsp"/>
	</body>
</html>