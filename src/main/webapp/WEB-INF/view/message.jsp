<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:include page="/WEB-INF/view/layout/layout.jsp">
<jsp:param name="title" value="新規投稿" />
<jsp:param name="content">
	<jsp:attribute name="value">

		<div id="main">
			<div class="messageForm">
				<form:form modelAttribute="messageForm"><br />
				<div class ="errorMessages"><form:errors path="*"  /></div>
		            <form:label path="title">件名(30文字以内)</form:label>
		            <form:input path="title"/> <br />

		            <form:label path="category">カテゴリ(10文字以内)</form:label>
		            <form:input path="category"/>  <br />

					<form:label path="text">投稿内容(1000文字以内)</form:label><br />
		      		<form:textarea path="text" cols="100" rows="10"/> <br />

		      		<form:input type="hidden" path="userId" value="${loginUser.id}"/>
		            <input type="submit" value="投稿" class="button"/> <br />
		       	</form:form>

	        </div>
        </div>

		</jsp:attribute>
	</jsp:param>
</jsp:include>