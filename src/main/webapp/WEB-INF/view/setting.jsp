<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:include page="/WEB-INF/view/layout/layout.jsp">
<jsp:param name="title" value="ユーザー編集" />
	<jsp:param name="content">
		<jsp:attribute name="value">

			<div id="main">

				<span class="signup">${message}</span><br /><br />

				<form:form modelAttribute="updateForm" action ="${pageContext.request.contextPath}/setting?userId=${updateForm.id}">

					<div class ="errorMessages"><form:errors path="*"  /></div>
					<div class ="errorMessages">${errorMessage}</div>

					<table>
						<tr>
							<th><form:label path="account">アカウント：</form:label></th>
						  	<td><form:input path="account" /></td>
			            </tr>
		            	<tr>
			        		<th><form:label path="password">パスワード：</form:label></th>
						  	<td><form:password path="password" /></td>
		                </tr>
		                <tr>
							<th><form:label path="confirmPassword">確認用パスワード：</form:label></th>
						  	<td><form:password path="confirmPassword" /></td>
		                </tr>
		                <tr>
			        		<th><form:label path="name">ユーザー名：</form:label></th>
						  	<td><form:input path="name" /></td>
		                </tr>

						<c:if test="${loginUser.id != userForm.id}">

						<tr>
							<th><form:label path="branchId">支社：</form:label></th>
						  	<td><form:select path="branchId" items="${branchFormList}" itemValue ="id" itemLabel="name"/></td>
		                </tr>
		                <tr>
							<th><form:label path="departmentId">部署：</form:label></th>
						  	<td><form:select path="departmentId" items="${departmentFormList}" itemValue ="id" itemLabel="name"/></td>
		                </tr>

						</c:if>
					</table>

					<input type="submit" value="更新" />
					<form:hidden path="id"/>

				</form:form>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>