<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/error/error.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<link href="<c:url value="/resources/css/login.css" />" rel="stylesheet">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログインページ</title>
	</head>
	<body>
		<div class="wrapper">
			<div class="main">
				<div>
					<h1>ログイン</h1>
				</div>

				<div class="errorMessages">
					<c:if test= "${not empty errorMessage}">
						<span>${errorMessage}</span><br/>
						<c:remove var="errorMessage" scope="session" />
					</c:if>
				</div>

			    <form:form modelAttribute="loginForm" method="post"	 action="${pageContext.request.contextPath}/login">
				<div class ="errorMessages"><form:errors path="*"  /></div>
					<table>
						<tr>
							<th><label for="account">ユーザー名：</label></th>
					      	<td><form:input path="account" type="text"/></td>
					    </tr>
						<tr>
							<th><label for="password">パスワード：</label></th>
					      	<td><form:input path="password" type="password"/></td>
						</tr>
					</table><br/>
					<form:button>ログイン</form:button>
				</form:form><br/>
			</div>
		<div class="push"></div>
		</div>
		<div id="footer">&copy; 2020 Bulletin Board Team</div>
	</body>
</html>