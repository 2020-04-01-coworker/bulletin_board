<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:include page="/WEB-INF/view/layout/layout.jsp">
<jsp:param name="title" value="ホーム" />
<jsp:param name="content">
	<jsp:attribute name="value">

		<div class="main-contents">
				<div class="homeContents">
					<div class="errorMessages">
				    	<c:if test= "${not empty errorMessages}">
							<span>${errorMessages}</span><br/>
							<c:remove var="errorMessages" scope="session" />
						</c:if>
					</div>

					<div class="searchForm">
						<form:form modelAttribute="searchForm" action="/BulletinboardBySpring/" method="GET">
							<form:input type="date" path="startDate" />
							<form:input type="date" path="endDate" />
							<form:input path="category"/>
							<input type="submit" value="絞り込み">
						</form:form>
					</div>

				    <c:forEach items="${allUserMessage}" var="message" varStatus="status">
						<div class="message">

				            <div class="name">ユーザー名：<c:out value="${message.name}"></c:out><br /></div>
				            <div class="title">タイトル：<c:out value="${message.title}"></c:out><br /></div>
				            <div class="category">カテゴリー：<c:out value="${message.category}"></c:out><br /></div>
				            <div class="text">内容：
					            <c:forEach var="splitedMessage" items="${fn:split(message.text, '
')}">
									<span>${splitedMessage}</span></br>
								</c:forEach>
							</div>

							<div class="deleteButton">
								<c:if test="${loginUser.id == message.userId}">
									<form:form modelAttribute="userMessageForm" action="/BulletinboardBySpring/deletemessage" method="POST">
										<form:hidden path="id" value="${message.id}"/>
										<input type="submit" value="投稿削除"/ id="deleteSubmit">
									 </form:form>
								 </c:if>
							</div>
				            <div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

							<div class="comment">
								<div class="commentTitle">＜　　コメント　　＞</div>
								<c:forEach items="${userComment}" var="comment">
									<c:if test="${ comment.messageId == message.id }">
										<div class="comments">
											<div class="commentName">ユーザー名：<c:out value="${comment.userName}" /></div><br />

						            		<div class = "comment">
												<c:forEach var="splitedComment" items="${fn:split(comment.text, '
')}">
												<span>${splitedComment}</span></br>
												</c:forEach>

												<div class="deleteButton">
													<c:if test="${loginUser.id == comment.userId}">
											          	 <form:form modelAttribute="deleteCommentForm" action="/BulletinboardBySpring/deletecomment" method="POST">
														    <form:input type="hidden" path ="id" value="${comment.id}"/>
														    <input type="submit" value="コメント削除" id="deleteSubmit">
													    </form:form>
												    </c:if>
											    </div>
								            	<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

											</div>
										</div>
									</c:if>
								 </c:forEach>
							</div>
							<div class="commentForm">
 				            	<form:form modelAttribute="commentForms"
 				            						 action="/BulletinboardBySpring/" method="POST">
					            	<div class ="errorMessages"><form:errors path="commentForm[${status.index}].text"  /></div>
  	 								<form:textarea path="commentForm[${status.index}].text" rows="5" cols="80"/>
  	 								<form:input type="hidden" path ="commentForm[${status.index}].messageId" value="${message.id}"/>
  	 								<form:input type="hidden" path ="commentForm[${status.index}].userId" value="${loginUser.id}"/>
 									<form:button name="comment">コメント送信</form:button>
								</form:form>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</jsp:attribute>
	</jsp:param>
</jsp:include>