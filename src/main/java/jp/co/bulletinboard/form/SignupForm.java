package jp.co.bulletinboard.form;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import jp.co.bulletinboard.validator.ConfirmPassword;
import jp.co.bulletinboard.validator.Unused;
import jp.co.bulletinboard.validator.ValidGroup1;
import jp.co.bulletinboard.validator.ValidGroup2;
import jp.co.bulletinboard.validator.ValidGroup3;
import jp.co.bulletinboard.validator.ValidGroup4;
import jp.co.bulletinboard.validator.ValidGroup5;

@ConfirmPassword(field = "password", groups = ValidGroup5.class)
public class SignupForm {

	private int id;

	@NotBlank(groups = ValidGroup1.class)
	@Size(max = 20, groups = ValidGroup2.class)
	@Length(min = 6, groups = ValidGroup3.class)
	@Pattern(regexp = "^[0-9a-zA-Z]+$", groups = ValidGroup4.class)
	@Unused(groups = ValidGroup5.class)
	private String account;

	@NotBlank(groups = ValidGroup1.class)
	@Size(max = 20, groups = ValidGroup2.class)
	@Length(min = 6, groups = ValidGroup3.class)
	@Pattern(regexp = "^[a-zA-Z0-9!-/:-@¥[-`{-~]]+$", groups = ValidGroup4.class)
	private String password;

	@NotBlank(groups = ValidGroup1.class)
	private String confirmPassword;

	@NotBlank(groups = ValidGroup1.class)
	@Size(max = 10, groups = ValidGroup3.class)
	private String name;

	private int branchId;
	private int departmentId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
}