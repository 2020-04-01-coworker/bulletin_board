package jp.co.bulletinboard.form;

import java.util.List;

public class CommentForms {

	private List<CommentForm> commentForm;

	public List<CommentForm> getCommentForm() {
		return this.commentForm;
	}

	public void setCommentForm(List<CommentForm> commentForm) {
		this.commentForm = commentForm;
	}

	@Override
	public String toString() {
		return "CommentForms [commentForm=" + this.commentForm + "]";
	}


}
