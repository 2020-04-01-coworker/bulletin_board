package jp.co.bulletinboard.form;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import jp.co.bulletinboard.validator.ValidGroup1;
import jp.co.bulletinboard.validator.ValidGroup2;

public class MessageForm implements Serializable {

	private int id;

	@NotBlank(groups= ValidGroup1.class)
	@Size(max=30,groups= ValidGroup2.class)
    private String title;

	@NotBlank(groups= ValidGroup1.class)
	@Size(max=1000,groups= ValidGroup2.class)
    private String text;

	@NotBlank(groups= ValidGroup1.class)
	@Size(max=10,groups= ValidGroup2.class)
    private String category;

    private int userId;
    private Date createdDate;
    private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
