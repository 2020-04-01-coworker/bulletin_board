package jp.co.bulletinboard.form;

import org.hibernate.validator.constraints.NotBlank;

public class LoginForm {

	@NotBlank
	private String account;

	@NotBlank
	private String password;


	public String getAccount() {

		return account;

	}

	public void setAccount(String account) {

		this.account = account;

	}

	public String getPassword() {

		return password;

	}

	public void setPassword(String password) {

		this.password = password;

	}

}