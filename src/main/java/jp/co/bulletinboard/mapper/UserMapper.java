package jp.co.bulletinboard.mapper;

import jp.co.bulletinboard.entity.User;

public interface UserMapper {

	User getUser(int userId);

	User getUserByAccount(String account);

	boolean existUserAccount(String account);

	boolean existUserId(int userId);

	void updateUser(User entity);

	void insertUser(User user);

	void updateStatement(User entity);

	void notContainsPassUpdateUser(User entity);
}
