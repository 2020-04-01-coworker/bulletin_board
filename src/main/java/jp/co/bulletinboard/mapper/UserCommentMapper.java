package jp.co.bulletinboard.mapper;

import java.util.List;

import jp.co.bulletinboard.entity.UserComment;

public interface UserCommentMapper {

	List<UserComment> getUserComment();

}
