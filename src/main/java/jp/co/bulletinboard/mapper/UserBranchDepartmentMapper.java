package jp.co.bulletinboard.mapper;

import java.util.List;

import jp.co.bulletinboard.entity.UserBranchDepartment;

public interface UserBranchDepartmentMapper {

	UserBranchDepartment getUserBranchDepartment();

	List<UserBranchDepartment> getUserBranchDepartmentAll();
}