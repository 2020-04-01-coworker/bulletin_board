package jp.co.bulletinboard.mapper;

import java.util.List;

import jp.co.bulletinboard.entity.Branch;

public interface BranchMapper {

	List<Branch> getBranches();


}