package jp.co.bulletinboard.mapper;

import jp.co.bulletinboard.dto.CommentDto;

public interface CommentMapper {

	void insertComment(CommentDto dto);
	void deleteComment(int id);

}
