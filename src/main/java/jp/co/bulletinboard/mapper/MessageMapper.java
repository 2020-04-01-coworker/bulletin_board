package jp.co.bulletinboard.mapper;

import jp.co.bulletinboard.dto.MessageDto;

public interface MessageMapper {

	void deleteMessage(int id);
	void insertMessage(MessageDto dto);

}
