package jp.co.bulletinboard.mapper;

import java.util.List;

import jp.co.bulletinboard.entity.Department;

public interface DepartmentMapper {

	List<Department> getDepartments();

}
