package jp.co.bulletinboard.mapper;

import java.util.List;

import jp.co.bulletinboard.dto.SearchDto;
import jp.co.bulletinboard.entity.UserMessage;

public interface UserMessageMapper {

	public abstract List<UserMessage> getUserMessageAll(SearchDto dto);

}
