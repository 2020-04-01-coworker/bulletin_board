package jp.co.bulletinboard.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jp.co.bulletinboard.dto.MessageDto;
import jp.co.bulletinboard.dto.SearchDto;
import jp.co.bulletinboard.dto.UserMessageDto;
import jp.co.bulletinboard.entity.UserMessage;
import jp.co.bulletinboard.form.MessageForm;
import jp.co.bulletinboard.form.SearchForm;
import jp.co.bulletinboard.form.UserMessageForm;
import jp.co.bulletinboard.mapper.MessageMapper;
import jp.co.bulletinboard.mapper.UserMessageMapper;

@Service
public class MessageService {
	@Autowired
    private UserMessageMapper userMessageMapper;
	@Autowired
	private MessageMapper messageMapper;

	public List<UserMessageForm> getUserMessageAll(SearchForm searchForm) {
		String start = searchForm.getStartDate();
		String end = searchForm.getEndDate();

		if(StringUtils.isEmpty(start)) {
        	searchForm.setStartDate("2020-01-01 00:00:00");
        } else {
        	searchForm.setStartDate(start + " 00:00:00");
        }

        if(StringUtils.isEmpty(end)) {
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	Date date = new Date();
        	searchForm.setEndDate(sdf.format(date));
        } else {
        	searchForm.setEndDate(end + " 23:59:59");
        }

        String category = searchForm.getCategory();
        if(StringUtils.isEmpty(searchForm.getCategory())) {
        	searchForm.setCategory("%%");
        }else{
        	searchForm.setCategory("%" + category + "%");

        }
        SearchDto searchDto = new SearchDto();
        BeanUtils.copyProperties(searchForm, searchDto);

        searchForm.setCategory(category);
        searchForm.setStartDate(start);
        searchForm.setEndDate(end);

	    List<UserMessage> userMessageList = userMessageMapper.getUserMessageAll(searchDto);
	    List<UserMessageDto> resultDto = convertToListDto(userMessageList);
	    List<UserMessageForm> resultForm = convertToListForm(resultDto);
	    return resultForm;
	}

	public void insertMessage(MessageForm messageForm) {
		MessageDto messageDto = new MessageDto();
		BeanUtils.copyProperties(messageForm, messageDto);
		messageMapper.insertMessage(messageDto);
	}

	public void deleteMessageService(UserMessageForm userMessageForm) {
		int id = userMessageForm.getId();
	    messageMapper.deleteMessage(id); //この行でDao呼び出し
	}


	public List<UserMessageDto> convertToListDto(List<UserMessage> selectedEntity){
		List<UserMessageDto> convertedDto = new LinkedList<UserMessageDto>();

        for (UserMessage bufferEntity : selectedEntity) {
            UserMessageDto bufferDto = new UserMessageDto();
            BeanUtils.copyProperties(bufferEntity, bufferDto);
            convertedDto.add(bufferDto);
        }
        return convertedDto;
	}

	public List<UserMessageForm> convertToListForm(List<UserMessageDto> listDto){
		List<UserMessageForm> convertedForm = new LinkedList<UserMessageForm>();

        for (UserMessageDto bufferDto : listDto) {
            UserMessageForm bufferForm = new UserMessageForm();
            BeanUtils.copyProperties(bufferDto, bufferForm);
            convertedForm.add(bufferForm);
        }
        return convertedForm;
	}
}
