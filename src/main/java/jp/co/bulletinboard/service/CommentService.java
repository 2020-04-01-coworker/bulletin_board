package jp.co.bulletinboard.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.bulletinboard.dto.CommentDto;
import jp.co.bulletinboard.dto.UserCommentDto;
import jp.co.bulletinboard.entity.UserComment;
import jp.co.bulletinboard.form.CommentForm;
import jp.co.bulletinboard.mapper.CommentMapper;
import jp.co.bulletinboard.mapper.UserCommentMapper;

@Service
public class CommentService {

	@Autowired
	private CommentMapper commentMapper;
	@Autowired
	private UserCommentMapper userCommentMapper;

	public void insertComment(CommentForm form) {
		CommentDto dto = new CommentDto();
		BeanUtils.copyProperties(form, dto);
		commentMapper.insertComment(dto);
	}

	public List<UserCommentDto> getUserComment() {
		List<UserComment> commentList = userCommentMapper.getUserComment();
		List<UserCommentDto> resultList = convertToDto(commentList);
		return resultList;
	}

	public void deleteComment(CommentForm form) {
		int id = form.getId();
		commentMapper.deleteComment(id);
	}

	public List<UserCommentDto> convertToDto(List<UserComment> commentList) {
		List<UserCommentDto> resutList = new LinkedList<UserCommentDto>();
		for(UserComment entity : commentList) {
			UserCommentDto dto = new UserCommentDto();
			BeanUtils.copyProperties(entity, dto);
			resutList.add(dto);
		}
	return resutList;
	}
}