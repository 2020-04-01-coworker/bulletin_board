package jp.co.bulletinboard.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.bulletinboard.dto.BranchDto;
import jp.co.bulletinboard.entity.Branch;
import jp.co.bulletinboard.form.BranchForm;
import jp.co.bulletinboard.mapper.BranchMapper;

@Service
public class BranchService {

	@Autowired
	public BranchMapper branchMapper;

	public List<BranchForm> getBranches(){

		List<Branch> branchList = branchMapper.getBranches();
		List<BranchDto> branchDtoList = convertToDto(branchList);
		List<BranchForm> branchFormList = convertToForm(branchDtoList);

		return branchFormList;
	}

	private List<BranchDto> convertToDto(List<Branch> branchList) {

        List<BranchDto> resultList = new LinkedList<BranchDto>();
        for (Branch entity : branchList) {
        	BranchDto dto = new BranchDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
	}

	private List<BranchForm> convertToForm(List<BranchDto> branchList) {

        List<BranchForm> resultList = new LinkedList<BranchForm>();
        for (BranchDto dto : branchList) {
        	BranchForm form  = new BranchForm();
            BeanUtils.copyProperties(dto,form);
            resultList.add(form);
        }
        return resultList;
	}
}