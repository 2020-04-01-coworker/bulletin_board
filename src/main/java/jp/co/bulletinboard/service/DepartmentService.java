package jp.co.bulletinboard.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.bulletinboard.dto.DepartmentDto;
import jp.co.bulletinboard.entity.Department;
import jp.co.bulletinboard.form.DepartmentForm;
import jp.co.bulletinboard.mapper.DepartmentMapper;

@Service
public class DepartmentService {

	@Autowired
	public DepartmentMapper departmentMapper;

	public List<DepartmentForm> getDepartments() {

		List<Department> departmentList = departmentMapper.getDepartments();
		List<DepartmentDto> departmentDtoList = convertToDto(departmentList);
		List<DepartmentForm> departmentFormList = convertToForm(departmentDtoList);
		return departmentFormList;
	}

	private List<DepartmentDto> convertToDto(List<Department> departmentList) {

		List<DepartmentDto> resultList = new LinkedList<DepartmentDto>();
		for (Department entity : departmentList) {
			DepartmentDto dto = new DepartmentDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	private List<DepartmentForm> convertToForm(List<DepartmentDto> departmentList) {

        List<DepartmentForm> resultList = new LinkedList<DepartmentForm>();
        for (DepartmentDto dto : departmentList) {
        	DepartmentForm form  = new DepartmentForm();
            BeanUtils.copyProperties(dto,form);
            resultList.add(form);
        }
        return resultList;
	}
}