package jp.co.bulletinboard.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import jp.co.bulletinboard.dto.UserBranchDepartmentDto;
import jp.co.bulletinboard.dto.UserDto;
import jp.co.bulletinboard.entity.User;
import jp.co.bulletinboard.entity.UserBranchDepartment;
import jp.co.bulletinboard.form.LoginForm;
import jp.co.bulletinboard.form.SignupForm;
import jp.co.bulletinboard.form.UpdateForm;
import jp.co.bulletinboard.form.UserBranchDepartmentForm;
import jp.co.bulletinboard.form.UserForm;
import jp.co.bulletinboard.mapper.UserBranchDepartmentMapper;
import jp.co.bulletinboard.mapper.UserMapper;


@Service
public class UserService {

	@Autowired
    private UserBranchDepartmentMapper userBranchDepartmentMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private PasswordEncoder passwordEncoder;

	public UserBranchDepartmentForm getUserBranchDepartment() {

		UserBranchDepartmentForm form = new UserBranchDepartmentForm();
		UserBranchDepartmentDto dto = new UserBranchDepartmentDto();
		UserBranchDepartment entity = userBranchDepartmentMapper.getUserBranchDepartment();
        BeanUtils.copyProperties(entity, dto);
        BeanUtils.copyProperties(dto, form);
        return form;
    }

	@Transactional
	public void updateUser(UpdateForm updateForm) {

		UserDto dto = new UserDto();
		User entity = new User();
		BeanUtils.copyProperties(updateForm,dto);

		if(StringUtils.isEmpty(dto.getPassword())) {
			BeanUtils.copyProperties(dto, entity);
			userMapper.notContainsPassUpdateUser(entity);
			return;
		}

		dto.setPassword(passwordEncoder.encode(dto.getPassword()));
		BeanUtils.copyProperties(dto, entity);
		userMapper.updateUser(entity);
	}

	public UpdateForm getUser(int userId) {

		UpdateForm form = new UpdateForm();
		UserDto dto = new UserDto();
		User entity = userMapper.getUser(userId);
		BeanUtils.copyProperties(entity, dto);
		BeanUtils.copyProperties(dto, form);

		return form;
	}

	public void insertUser(SignupForm signupForm) {

		UserDto userDto = new UserDto();
		User user = new User();

		BeanUtils.copyProperties(signupForm, userDto);

		String encordPassword = passwordEncoder.encode(signupForm.getPassword());
		userDto.setPassword(encordPassword);

		BeanUtils.copyProperties(userDto,user);

		userMapper.insertUser(user);

	}

	public UserDto authUser(LoginForm loginForm) {

		UserDto dto = new UserDto();
		User entity = userMapper.getUserByAccount(loginForm.getAccount());

		if(entity == null) {
			return null;
		}

		BeanUtils.copyProperties(entity, dto);

		if( ! passwordEncoder.matches(loginForm.getPassword(), dto.getPassword())) {
			return null;
		}

		return dto;
	}

	public boolean existUserAccount(String account) {
		return userMapper.existUserAccount(account);
	}

	public boolean existUserId(int userId) {
		return userMapper.existUserId(userId);
	}

	public List<UserBranchDepartmentDto> getUserBranchDepartmentAll() {
	    List<UserBranchDepartment> userBranchDepartmentList = userBranchDepartmentMapper.getUserBranchDepartmentAll();
	    List<UserBranchDepartmentDto> resultList = convertToDto(userBranchDepartmentList);
	    return resultList;
	}

	private List<UserBranchDepartmentDto> convertToDto(List<UserBranchDepartment> userBranchDepartmentList) {
	    List<UserBranchDepartmentDto> resultList = new LinkedList<UserBranchDepartmentDto>();
	    for (UserBranchDepartment entity : userBranchDepartmentList) {
	        UserBranchDepartmentDto dto = new UserBranchDepartmentDto();
	        BeanUtils.copyProperties(entity, dto);
	        resultList.add(dto);
	    }
	    return resultList;
	}

	@Transactional
	public void updateStatement(UserForm userForm) {

		UserDto dto = new UserDto();
 		User entity = new User();
 		BeanUtils.copyProperties(userForm,dto);
 		BeanUtils.copyProperties(dto, entity);
 		userMapper.updateStatement(entity);
	}
}