package jp.co.bulletinboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.bulletinboard.form.MessageForm;
import jp.co.bulletinboard.service.MessageService;
import jp.co.bulletinboard.validator.GroupOrder;

@Controller
public class MessageController {

	@Autowired
	private MessageService messageService;

	@RequestMapping(value = "/message", method = RequestMethod.GET)
	public String getMessage(Model model) {
		MessageForm form = new MessageForm();
		model.addAttribute("messageForm", form);
		//model.addAttribute("loginUser",session.getAttribute("loginUser"));
		return "message";
	}

	@RequestMapping(value = "/message", method = RequestMethod.POST)
	public String insertMessage(@Validated(GroupOrder.class) @ModelAttribute MessageForm form,BindingResult result, Model model) {

		if(result.hasErrors()) {
			return "message";
		}

		messageService.insertMessage(form);
		return "redirect:/";
	}
}
