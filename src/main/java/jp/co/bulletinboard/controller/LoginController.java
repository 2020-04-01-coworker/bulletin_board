
package jp.co.bulletinboard.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.bulletinboard.dto.UserDto;
import jp.co.bulletinboard.form.LoginForm;
import jp.co.bulletinboard.service.UserService;
import jp.co.bulletinboard.validator.LoginValidator;

@Controller
public class LoginController {

	@Autowired
	private HttpSession session;

	@Autowired
	private UserService userService;

	@Autowired
	private LoginValidator loginValidator;

	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(loginValidator);
    }

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	String getLogin(@ModelAttribute("loginForm") LoginForm loginForm) {
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	String login(@Valid @ModelAttribute("loginForm") LoginForm loginForm, BindingResult result, Model model) {

		if(result.hasErrors()) {
			loginForm.setPassword("");
			return getLogin(loginForm);
		}

		UserDto dto = userService.authUser(loginForm);
		session.setAttribute("loginUser", dto);

		return "redirect:/";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	String logout() {
		session.invalidate();
		return "redirect:login";
	}

}
