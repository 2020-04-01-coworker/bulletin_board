package jp.co.bulletinboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.bulletinboard.dto.UserCommentDto;
import jp.co.bulletinboard.form.CommentForm;
import jp.co.bulletinboard.form.CommentForms;
import jp.co.bulletinboard.form.SearchForm;
import jp.co.bulletinboard.form.UserMessageForm;
import jp.co.bulletinboard.service.CommentService;
import jp.co.bulletinboard.service.MessageService;
import jp.co.bulletinboard.validator.CommentValidator;

@Controller
public class HomeController {

	@Autowired
	private MessageService messageService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private CommentValidator commentValidator;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(commentValidator);
	}

	@ModelAttribute("commentForms")
	public CommentForms createCommentForms() {
		return new CommentForms();
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getUserMessageAll(@ModelAttribute(value = "searchForm") SearchForm searchForm, Model model) {

		List<UserMessageForm> allUserMessage = messageService.getUserMessageAll(searchForm);
		List<UserCommentDto> userComment = commentService.getUserComment();
		UserMessageForm userMessageForm = new UserMessageForm();
		CommentForm deleteCommentForm = new CommentForm();

		model.addAttribute("deleteCommentForm", deleteCommentForm);
		model.addAttribute("allUserMessage", allUserMessage);
		model.addAttribute("userComment", userComment);
		model.addAttribute("userMessageForm", userMessageForm);

		return "home";
	}

	@RequestMapping(value = "/deletemessage", method = RequestMethod.POST)
	public String deleteMessage(@ModelAttribute UserMessageForm userMessageForm, Model model) {

		messageService.deleteMessageService(userMessageForm);
		return "redirect:/";
	}

	@RequestMapping(value = "/deletecomment", method = RequestMethod.POST)
	public String deleteComment(@ModelAttribute CommentForm deleteCommentForm, Model model)
			throws IllegalAccessException {

		commentService.deleteComment(deleteCommentForm);
		return "redirect:/";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST, params = "comment")
	public String insertComment(
			@Validated @ModelAttribute(value = "commentForms") CommentForms commentForms,
			BindingResult result,
			Model model) {

		if (result.hasErrors()) {
			SearchForm searchForm = new SearchForm();
			List<UserMessageForm> allUserMessage = messageService.getUserMessageAll(searchForm);
			List<UserCommentDto> userComment = commentService.getUserComment();
			UserMessageForm userMessageForm = new UserMessageForm();
			CommentForm deleteCommentForm = new CommentForm();

			model.addAttribute("searchForm", searchForm);
			model.addAttribute("deleteCommentForm", deleteCommentForm);
			model.addAttribute("allUserMessage", allUserMessage);
			model.addAttribute("userComment", userComment);
			model.addAttribute("userMessageForm", userMessageForm);

			return "home";
		}

		commentService.insertComment(getCommentForm(commentForms));
		return "redirect:/";
	}

	private CommentForm getCommentForm(CommentForms commentForms) {

		CommentForm commentForm = new CommentForm();
		int count = 0;
		for (CommentForm cf : commentForms.getCommentForm()) {
			if (cf.getText() != null) {
				commentForm.setId(count);
				commentForm.setText(cf.getText());
				commentForm.setUserId(cf.getUserId());
				commentForm.setMessageId(cf.getMessageId());
			}
			count++;
		}

		return commentForm;
	}

}
