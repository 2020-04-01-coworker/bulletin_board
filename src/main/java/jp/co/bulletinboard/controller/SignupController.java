package jp.co.bulletinboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.bulletinboard.form.BranchForm;
import jp.co.bulletinboard.form.DepartmentForm;
import jp.co.bulletinboard.form.SignupForm;
import jp.co.bulletinboard.service.BranchService;
import jp.co.bulletinboard.service.DepartmentService;
import jp.co.bulletinboard.service.UserService;
import jp.co.bulletinboard.validator.GroupOrder;

@Controller
public class SignupController {

	@Autowired
	UserService userService;

	@Autowired
	BranchService branchService;

	@Autowired
	DepartmentService departmentService;

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String getSignup(Model model) {

		SignupForm signupForm = new SignupForm();
		List<BranchForm> branches = branchService.getBranches();
		List<DepartmentForm> departments = departmentService.getDepartments();

		model.addAttribute("signupForm", signupForm);
		model.addAttribute("branches", branches);
		model.addAttribute("departments", departments);

		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String insertUser(@Validated(GroupOrder.class) @ModelAttribute SignupForm signupForm, BindingResult result, Model model) {


		if(result.hasErrors()) {

			if(! isValid(signupForm)) {
				model.addAttribute("errorMessage","支社と部署の組み合わせが不正です");
			}

			List<BranchForm> branches = branchService.getBranches();
			List<DepartmentForm> departments = departmentService.getDepartments();

			model.addAttribute("branches", branches);
			model.addAttribute("departments", departments);

			return "signup";
		}

		if(! isValid(signupForm)) {

			List<BranchForm> branches = branchService.getBranches();
			List<DepartmentForm> departments = departmentService.getDepartments();

			model.addAttribute("branches", branches);
			model.addAttribute("departments", departments);
			model.addAttribute("errorMessage","支社と部署の組み合わせが間違っています");

			return "signup";
		}

		userService.insertUser(signupForm);
		return "redirect:management";
	}



	//支社部署の組み合わせチェック用
	private boolean isValid(SignupForm form) {

		int branchId = form.getBranchId();
		int departmentId = form.getDepartmentId();

		//2 ＝ A社、 3 ＝ B社、 4 ＝ C社
		if ((branchId == 2) || (branchId == 3) || (branchId == 4)) {

			// 1 ＝ 総務人事部、 2 ＝ 情報管理部
			if ((departmentId == 1) || (departmentId == 2)) {
				return false;
			}

			//1 = 本社
		} else if (branchId == 1) {

			// 3 ＝ 営業部、 4 ＝ 技術部
			if ((departmentId == 3) || (departmentId == 4)) {

				return false;
			}
		}
		return true;
	}
}
