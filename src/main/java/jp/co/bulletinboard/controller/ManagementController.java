package jp.co.bulletinboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.bulletinboard.dto.UserBranchDepartmentDto;
import jp.co.bulletinboard.form.UserForm;
import jp.co.bulletinboard.service.UserService;


@Controller
public class ManagementController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/management", method = RequestMethod.GET)
	public String userBranchDepartmentAll(Model model) {
	    List<UserBranchDepartmentDto> allUsers = userService.getUserBranchDepartmentAll();
	    model.addAttribute("users", allUsers);
	    
	    UserForm form = new UserForm();
        model.addAttribute("userForm", form);
	    return "management";
	}

	@RequestMapping(value = "/management/update", method = RequestMethod.POST)
	public String statusUpdate(@ModelAttribute UserForm userForm, Model model) {
		userService.updateStatement(userForm);
		return "redirect:/management";
	}
}