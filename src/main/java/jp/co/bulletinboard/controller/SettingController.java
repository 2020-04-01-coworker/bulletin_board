package jp.co.bulletinboard.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.bulletinboard.dto.UserDto;
import jp.co.bulletinboard.form.BranchForm;
import jp.co.bulletinboard.form.DepartmentForm;
import jp.co.bulletinboard.form.UpdateForm;
import jp.co.bulletinboard.service.BranchService;
import jp.co.bulletinboard.service.DepartmentService;
import jp.co.bulletinboard.service.UserService;
import jp.co.bulletinboard.validator.GroupOrder;
import jp.co.bulletinboard.validator.UsedAccountValidator;

@Controller
public class SettingController {

	@Autowired
	private UserService userService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private UsedAccountValidator usedAccountValidator;

	@Autowired
	private HttpSession session;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(usedAccountValidator);
	}

	@RequestMapping(value = "/setting", method = RequestMethod.GET)
	public String getEditUser(Model model, @RequestParam(value = "userId", defaultValue = "") String userId,
			RedirectAttributes redirectAttributes) {

		Integer parseUserId = checkUserIdParam(userId);

		if (parseUserId == null) {
			redirectAttributes.addFlashAttribute("errorMessage", "不正なパラメータが入力されました");
			return "redirect:/management";
		}

		UpdateForm updateForm = userService.getUser(parseUserId);
		List<BranchForm> branchFormList = new ArrayList<>();
		List<DepartmentForm> departmentFormList = new ArrayList<>();

		UserDto loginUser = ((UserDto) session.getAttribute("loginUser"));

		if (loginUser.getId() == parseUserId) {
			branchFormList.add(branchService.getBranches().get(0));
			departmentFormList.add(departmentService.getDepartments().get(0));
		} else {
			branchFormList = branchService.getBranches();
			departmentFormList = departmentService.getDepartments();
		}

		model.addAttribute("branchFormList", branchFormList);
		model.addAttribute("departmentFormList", departmentFormList);
		model.addAttribute("updateForm", updateForm);
		model.addAttribute("message", "ユーザー編集");

		return "setting";
	}

	//ユーザー更新
	@RequestMapping(value = "/setting", method = RequestMethod.POST)
	public String updateUser(@Validated(GroupOrder.class) @ModelAttribute("updateForm") UpdateForm updateForm,
			BindingResult result, Model model) {

		BindingResult filteringBindingResult = filteringBindingResult(result, updateForm);

		if (filteringBindingResult.hasErrors()) {

			if (!isValid(updateForm)) {
				model.addAttribute("errorMessage", "支社と部署の組み合わせが不正です");
			}

			List<BranchForm> branchFormList = branchService.getBranches();
			List<DepartmentForm> departmentFormList = departmentService.getDepartments();

			model.addAttribute("message", "エラー");
			model.addAttribute("departmentFormList", departmentFormList);
			model.addAttribute("branchFormList", branchFormList);
			model.addAttribute("org.springframework.validation.BindingResult.updateForm", filteringBindingResult);

			return "setting";
		}

		if (!isValid(updateForm)) {
			model.addAttribute("errorMessage", "支社と部署の組み合わせが間違っています");

			List<BranchForm> branchFormList = branchService.getBranches();
			List<DepartmentForm> departmentFormList = departmentService.getDepartments();

			model.addAttribute("message", "エラー");
			model.addAttribute("departmentFormList", departmentFormList);
			model.addAttribute("branchFormList", branchFormList);
			model.addAttribute("org.springframework.validation.BindingResult.updateForm", filteringBindingResult);

			return "setting";
		}

		userService.updateUser(updateForm);
		return "redirect:management";
	}

	//支社部署の組み合わせチェック用
	private boolean isValid(UpdateForm form) {

		UserDto loginUser = (UserDto) session.getAttribute("loginUser");

		if (loginUser.getDepartmentId() == 1 && form.getDepartmentId() != 1) {
			return false;
		}

		int branchId = form.getBranchId();
		int departmentId = form.getDepartmentId();

		//2 ＝ A社、 3 ＝ B社、 4 ＝ C社
		if ((branchId == 2) || (branchId == 3) || (branchId == 4)) {

			// 1 ＝ 総務人事部、 2 ＝ 情報管理部
			if ((departmentId == 1) || (departmentId == 2)) {
				return false;
			}

			//1 = 本社
		} else if (branchId == 1) {

			// 3 ＝ 営業部、 4 ＝ 技術部
			if ((departmentId == 3) || (departmentId == 4)) {

				return false;
			}
		}
		return true;
	}

	//パスワード入力が空だった時にバリデーションを無効にするメソッド
	private BindingResult filteringBindingResult(BindingResult result, UpdateForm form) {

		BindingResult tmpResult = new BeanPropertyBindingResult(form, "updateForm");

		if (StringUtils.isEmpty(form.getPassword()) && StringUtils.isEmpty(form.getConfirmPassword())) {

			for (FieldError fieldError : result.getFieldErrors()) {
				if (fieldError.getField().equals("confirmPassword") ||
						fieldError.getField().equals("password")) {
					continue;
				}
				tmpResult.addError(fieldError);
			}
			return tmpResult;
		}

		return result;
	}

	private Integer checkUserIdParam(String userId) {

		if (!userId.matches("^[0-9]*$") || StringUtils.isEmpty(userId)) {
			return null;
		}

		Integer parseUserId = Integer.valueOf(userId);

		if (!userService.existUserId(parseUserId)) {
			return null;
		}

		return parseUserId;
	}
}