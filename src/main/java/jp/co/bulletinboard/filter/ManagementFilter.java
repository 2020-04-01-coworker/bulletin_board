package jp.co.bulletinboard.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import jp.co.bulletinboard.dto.UserDto;


@WebFilter(urlPatterns={"/setting", "/management", "/signup"})
@Component
public class ManagementFilter implements Filter {

	@Override

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();

		HttpServletResponse httpRes = (HttpServletResponse) response;

		String errorMessage = "アクセス権限がありません";

		UserDto user = (UserDto) session.getAttribute("loginUser");

		if (!isAuthorityAccount(user)) {

			session.setAttribute("errorMessages", errorMessage);
			httpRes.sendRedirect("./");
		}else {
			chain.doFilter(request, response);
		}

	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}


	private boolean isAuthorityAccount(UserDto user) {

		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if (branchId == 1 && departmentId == 1) {
			return true;
		}
		return false;
	}

}
