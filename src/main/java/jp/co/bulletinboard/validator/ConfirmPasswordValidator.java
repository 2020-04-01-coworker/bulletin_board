package jp.co.bulletinboard.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

public class ConfirmPasswordValidator implements ConstraintValidator<ConfirmPassword, Object> {

	private String field;
    private String confirmField;
    private String message;

    public void initialize(ConfirmPassword constraintAnnotation) {
    	
    	//アノテーションの引数
        this.field = constraintAnnotation.field();
        this.confirmField = "confirm" + StringUtils.capitalize(field);
        this.message = constraintAnnotation.message();
    }

    public boolean isValid(Object value, ConstraintValidatorContext context) {
    	
    	// 2つの値を取得して比較する。
        BeanWrapper beanWrapper = new BeanWrapperImpl(value);
        Object fieldValue = beanWrapper.getPropertyValue(field);
        Object confirmFieldValue = beanWrapper.getPropertyValue(confirmField);
        
        //比較処理
        boolean matched = ObjectUtils.nullSafeEquals(fieldValue, confirmFieldValue);

        if (matched) {
            return true;

        } else {
        	
        	//メッセージを設定
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message)
                    .addPropertyNode(field).addConstraintViolation();
            return false;
        }
    }
}