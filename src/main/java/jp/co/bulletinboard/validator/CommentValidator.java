package jp.co.bulletinboard.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import jp.co.bulletinboard.form.CommentForm;
import jp.co.bulletinboard.form.CommentForms;

@Component
public class CommentValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		CommentForms commentForms = (CommentForms) target;

		CommentForm commentForm = new CommentForm();
		int count = 0;
		for (CommentForm cf : commentForms.getCommentForm()) {
			if (cf.getText() != null) {
				commentForm.setId(count);
				commentForm.setText(cf.getText());
				commentForm.setUserId(cf.getUserId());
				commentForm.setMessageId(cf.getMessageId());
			}
			count++;
		}

		if (StringUtils.isBlank(commentForm.getText())) {
			errors.rejectValue("commentForm[" + commentForm.getId() + "].text",
					"CommentValidator.commentForms.commentForm[" + commentForm.getId() + "].text",
					"コメントを入力してください");
			return;
		}

		if (500 < commentForm.getText().length()) {
			errors.rejectValue("commentForm[" + commentForm.getId() + "].text",
					"CommentValidator.commentForms.commentForm[" + commentForm.getId() + "].text",
					"本文は500文字以下で入力してください");
			return;
		}

	}
}