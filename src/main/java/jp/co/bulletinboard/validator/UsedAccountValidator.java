package jp.co.bulletinboard.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import jp.co.bulletinboard.form.UpdateForm;
import jp.co.bulletinboard.service.UserService;



@Component
public class UsedAccountValidator implements Validator {

	@Autowired
	private UserService userService;

	@Override
	public boolean supports(Class<?> clazz) {
		return UpdateForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		UpdateForm updateForm = (UpdateForm) target;

		if (StringUtils.isEmpty(updateForm.getAccount())) {
			return;
		}

		if (updateForm.getAccount().equals(userService.getUser(updateForm.getId()).getAccount())) {
			return;
		}

		if (userService.existUserAccount(updateForm.getAccount())) {
			errors.rejectValue("account",
					"UsedAccountValidator.UpdateForm.account",
					"アカウントが重複しています");
		}
	}
}
