package jp.co.bulletinboard.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.bulletinboard.service.UserService;

public class UnusedValidator implements ConstraintValidator<Unused, String> {

	@Autowired
	private UserService userService;

	public void initialize(Unused constraintAnnotation) {
	}

	public boolean isValid(String value, ConstraintValidatorContext context) {

		if (!userService.existUserAccount(value)) {
			return true;
		}
		return false;
	}
}