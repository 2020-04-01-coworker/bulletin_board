package jp.co.bulletinboard.validator;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import jp.co.bulletinboard.dto.UserDto;
import jp.co.bulletinboard.form.LoginForm;
import jp.co.bulletinboard.service.UserService;



@Component
public class LoginValidator implements Validator {

	@Autowired
	private UserService userService;

	@Override
	public boolean supports(Class<?> clazz) {
		return LoginForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		LoginForm loginForm = (LoginForm) target;

		if (StringUtils.isEmpty(loginForm.getAccount())) {
			return;
		}

		UserDto dto = userService.authUser(loginForm);

		if(dto == null || dto.getIsStopped() == 1) {
			errors.rejectValue("account",
					"LoginValidator.LoginForm.account",
			"アカウントまたはパスワードが誤っています");
			return;
		}

	}
}