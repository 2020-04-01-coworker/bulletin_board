package jp.co.bulletinboard.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import jp.co.bulletinboard.dto.UserBranchDepartmentDto;
import jp.co.bulletinboard.form.UpdateForm;
import jp.co.bulletinboard.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mvc-config.xml")
@WebAppConfiguration
@PropertySource("classpath:ValidatorMessages_ja.properties")
@Transactional
public class SignupControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		executeScript("/META-INF/sql/setupSignupTest.sql");

	}

	//	@Test
	//	public void signupShow() throws Exception {
	//
	//		mockMvc.perform(get("/signup"))
	//				.andDo(print())
	//				.andExpect(status().isOk());
	//	}

	@Test
	public void test_50_新規会員登録_正常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account50";
		String password = "Pass$#50";
		String confirmPassword = "Pass$#50";
		String name = "テスト50";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "redirect:management";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		UpdateForm updateForm = new UpdateForm();
		List<UserBranchDepartmentDto> userBranchDepartmentDtos = userService.getUserBranchDepartmentAll();

		for (UserBranchDepartmentDto userBranchDepartmentDto : userBranchDepartmentDtos) {

			if (userBranchDepartmentDto.getAccount().equals(account)) {
				updateForm = userService.getUser(userBranchDepartmentDto.getId());
			}
		}

		assertThat(updateForm.getAccount(), is(account));
		assertTrue(passwordEncoder.matches(password, updateForm.getPassword()));
		assertThat(updateForm.getName(), is(name));
		assertThat(Integer.toString(updateForm.getBranchId()), is(branchId));
		assertThat(Integer.toString(updateForm.getDepartmentId()), is(departmentId));

		assertThat(mav.getViewName(), is(responseUrl));

	}

	@Test
	public void test_51_新規会員登録_正常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account51";
		String password = "Pass$#51";
		String confirmPassword = "Pass$#51";
		String name = "テスト51";
		String branchId = "1";
		String departmentId = "2";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "redirect:management";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		UpdateForm updateForm = new UpdateForm();
		List<UserBranchDepartmentDto> userBranchDepartmentDtos = userService.getUserBranchDepartmentAll();

		for (UserBranchDepartmentDto userBranchDepartmentDto : userBranchDepartmentDtos) {

			if (userBranchDepartmentDto.getAccount().equals(account)) {
				updateForm = userService.getUser(userBranchDepartmentDto.getId());
			}
		}

		assertThat(updateForm.getAccount(), is(account));
		assertTrue(passwordEncoder.matches(password, updateForm.getPassword()));
		assertThat(updateForm.getName(), is(name));
		assertThat(Integer.toString(updateForm.getBranchId()), is(branchId));
		assertThat(Integer.toString(updateForm.getDepartmentId()), is(departmentId));

		assertThat(mav.getViewName(), is(responseUrl));

	}

	@Test
	public void test_52_新規会員登録_正常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account52";
		String password = "Pass$#52";
		String confirmPassword = "Pass$#52";
		String name = "テスト52";
		String branchId = "2";
		String departmentId = "3";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "redirect:management";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		UpdateForm updateForm = new UpdateForm();
		List<UserBranchDepartmentDto> userBranchDepartmentDtos = userService.getUserBranchDepartmentAll();

		for (UserBranchDepartmentDto userBranchDepartmentDto : userBranchDepartmentDtos) {

			if (userBranchDepartmentDto.getAccount().equals(account)) {
				updateForm = userService.getUser(userBranchDepartmentDto.getId());
			}
		}

		assertThat(updateForm.getAccount(), is(account));
		assertTrue(passwordEncoder.matches(password, updateForm.getPassword()));
		assertThat(updateForm.getName(), is(name));
		assertThat(Integer.toString(updateForm.getBranchId()), is(branchId));
		assertThat(Integer.toString(updateForm.getDepartmentId()), is(departmentId));

		assertThat(mav.getViewName(), is(responseUrl));

	}

	@Test
	public void test_53_新規会員登録_正常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account53";
		String password = "Pass$#53";
		String confirmPassword = "Pass$#53";
		String name = "テスト53";
		String branchId = "3";
		String departmentId = "3";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "redirect:management";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		UpdateForm updateForm = new UpdateForm();
		List<UserBranchDepartmentDto> userBranchDepartmentDtos = userService.getUserBranchDepartmentAll();

		for (UserBranchDepartmentDto userBranchDepartmentDto : userBranchDepartmentDtos) {

			if (userBranchDepartmentDto.getAccount().equals(account)) {
				updateForm = userService.getUser(userBranchDepartmentDto.getId());
			}
		}

		assertThat(updateForm.getAccount(), is(account));
		assertTrue(passwordEncoder.matches(password, updateForm.getPassword()));
		assertThat(updateForm.getName(), is(name));
		assertThat(Integer.toString(updateForm.getBranchId()), is(branchId));
		assertThat(Integer.toString(updateForm.getDepartmentId()), is(departmentId));

		assertThat(mav.getViewName(), is(responseUrl));

	}

	@Test
	public void test_54_新規会員登録_正常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account54";
		String password = "Pass$#54";
		String confirmPassword = "Pass$#54";
		String name = "テスト54";
		String branchId = "4";
		String departmentId = "3";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "redirect:management";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		UpdateForm updateForm = new UpdateForm();
		List<UserBranchDepartmentDto> userBranchDepartmentDtos = userService.getUserBranchDepartmentAll();

		for (UserBranchDepartmentDto userBranchDepartmentDto : userBranchDepartmentDtos) {

			if (userBranchDepartmentDto.getAccount().equals(account)) {
				updateForm = userService.getUser(userBranchDepartmentDto.getId());
			}
		}

		assertThat(updateForm.getAccount(), is(account));
		assertTrue(passwordEncoder.matches(password, updateForm.getPassword()));
		assertThat(updateForm.getName(), is(name));
		assertThat(Integer.toString(updateForm.getBranchId()), is(branchId));
		assertThat(Integer.toString(updateForm.getDepartmentId()), is(departmentId));

		assertThat(mav.getViewName(), is(responseUrl));

	}

	@Test
	public void test_55_新規会員登録_正常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account55";
		String password = "Pass$#55";
		String confirmPassword = "Pass$#55";
		String name = "テスト55";
		String branchId = "2";
		String departmentId = "4";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "redirect:management";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		UpdateForm updateForm = new UpdateForm();
		List<UserBranchDepartmentDto> userBranchDepartmentDtos = userService.getUserBranchDepartmentAll();

		for (UserBranchDepartmentDto userBranchDepartmentDto : userBranchDepartmentDtos) {

			if (userBranchDepartmentDto.getAccount().equals(account)) {
				updateForm = userService.getUser(userBranchDepartmentDto.getId());
			}
		}

		assertThat(updateForm.getAccount(), is(account));
		assertTrue(passwordEncoder.matches(password, updateForm.getPassword()));
		assertThat(updateForm.getName(), is(name));
		assertThat(Integer.toString(updateForm.getBranchId()), is(branchId));
		assertThat(Integer.toString(updateForm.getDepartmentId()), is(departmentId));

		assertThat(mav.getViewName(), is(responseUrl));

	}

	@Test
	public void test_56_新規会員登録_正常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account56";
		String password = "Pass$#56";
		String confirmPassword = "Pass$#56";
		String name = "テスト56";
		String branchId = "3";
		String departmentId = "4";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "redirect:management";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		UpdateForm updateForm = new UpdateForm();
		List<UserBranchDepartmentDto> userBranchDepartmentDtos = userService.getUserBranchDepartmentAll();

		for (UserBranchDepartmentDto userBranchDepartmentDto : userBranchDepartmentDtos) {

			if (userBranchDepartmentDto.getAccount().equals(account)) {
				updateForm = userService.getUser(userBranchDepartmentDto.getId());
			}
		}

		assertThat(updateForm.getAccount(), is(account));
		assertTrue(passwordEncoder.matches(password, updateForm.getPassword()));
		assertThat(updateForm.getName(), is(name));
		assertThat(Integer.toString(updateForm.getBranchId()), is(branchId));
		assertThat(Integer.toString(updateForm.getDepartmentId()), is(departmentId));

		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_57_新規会員登録_正常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account57";
		String password = "Pass$#57";
		String confirmPassword = "Pass$#57";
		String name = "テスト57";
		String branchId = "4";
		String departmentId = "4";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "redirect:management";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		UpdateForm updateForm = new UpdateForm();
		List<UserBranchDepartmentDto> userBranchDepartmentDtos = userService.getUserBranchDepartmentAll();

		for (UserBranchDepartmentDto userBranchDepartmentDto : userBranchDepartmentDtos) {

			if (userBranchDepartmentDto.getAccount().equals(account)) {
				updateForm = userService.getUser(userBranchDepartmentDto.getId());
			}
		}

		assertThat(updateForm.getAccount(), is(account));
		assertTrue(passwordEncoder.matches(password, updateForm.getPassword()));
		assertThat(updateForm.getName(), is(name));
		assertThat(Integer.toString(updateForm.getBranchId()), is(branchId));
		assertThat(Integer.toString(updateForm.getDepartmentId()), is(departmentId));

		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_58_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "58"; // 6文字以下でエラー
		String password = "Pass$#58";
		String confirmPassword = "Pass$#58";
		String name = "テスト58";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "length must be between 6 and 2147483647";
		//		String errorMessage = "アカウントは6文字以上で入力してください";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_59_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "acount59_012345678901"; // 20文字以上でエラー
		String password = "Pass$#59";
		String confirmPassword = "Pass$#59";
		String name = "テスト59";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "size must be between 0 and 20";
		//		String errorMessage = "アカウントは20文字以下で入力してください";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_60_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account"; // すでにあるアカウントと同一のためエラー
		String password = "Pass$#60";
		String confirmPassword = "Pass$#60";
		String name = "テスト60";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "アカウントが重複しています";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_61_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account61";
		String password = "P$#61"; // パスワードが５文字以下でエラー
		String confirmPassword = "P$#61";
		String name = "テスト61";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		//		String errorMessage = "パスワードは6文字以上20文字以下で入力してください";
		String errorMessage = "length must be between 6 and 2147483647";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_62_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account62";
		String password = "Pass$#62_012345678901"; // パスワードが20文字以下でエラー
		String confirmPassword = "Pass$#62_012345678901";
		String name = "テスト62";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		//		String errorMessage = "パスワードは6文字以上20文字以下で入力してください";
		String errorMessage = "size must be between 0 and 20";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_63_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account63";
		String password = "Pass$#63"; // passとconfirmPasswordで違いがある。
		String confirmPassword = "Pass$#62";
		String name = "テスト63";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "入力したパスワードと確認用パスワードが一致しません";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_64_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account64";
		String password = "Pass$#64";
		String confirmPassword = "Pass$#64";
		String name = "テスト64_01234"; // nameが11文字以上
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		//		String errorMessage = "ユーザー名は10文字以下で入力してください";
		String errorMessage = "size must be between 0 and 10";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");

		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_65_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "acc65"; // coountが5文字以下
		String password = "P$#65"; // passwordが5文字以下
		String confirmPassword = "P$#65";
		String name = "テスト65_01234"; // nameが11文字以上
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		//		String errorMessage1 = "アカウントは6文字以上で入力してください";
		//		String errorMessage2 = "パスワードは6文字以上で入力してください";
		//		String errorMessage3 = "ユーザー名は10文字以下で入力してください";

		String errorMessage1 = "length must be between 6 and 2147483647";
		String errorMessage2 = "length must be between 6 and 2147483647";
		String errorMessage3 = "size must be between 0 and 10";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		List<String> errorMessages = new ArrayList<>();

		for (ObjectError error : errors.getAllErrors()) {
			errorMessages.add(error.getDefaultMessage());
		}

		assertTrue(errorMessages.contains(errorMessage1));
		assertTrue(errorMessages.contains(errorMessage2));
		assertTrue(errorMessages.contains(errorMessage3));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_66_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = ""; // accountが空
		String password = "Pass$#66";
		String confirmPassword = "Pass$#66";
		String name = "テスト66";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		//		String errorMessage = "アカウントを入力してください";
		String errorMessage = "may not be empty";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_67_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account67";
		String password = ""; // passwordが空
		String confirmPassword = "Pass$#67";
		String name = "テスト67";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		//		String errorMessage = "パスワードを入力してください";
		String errorMessage = "may not be empty";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_68_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account68";
		String password = "Pass$#68";
		String confirmPassword = "";// confirmPasswordが空
		String name = "テスト68";
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		//		String errorMessage = "入力したパスワードと確認用パスワードが一致しません";
		String errorMessage = "may not be empty";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_69_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account69";
		String password = "Pass$#69";
		String confirmPassword = "Pass$#69";
		String name = ""; // confirmPasswordが空
		String branchId = "1";
		String departmentId = "1";

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		//		String errorMessage = "ユーザー名を入力してください";
		String errorMessage = "may not be empty";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		Errors errors = (Errors) mav.getModel().get(BindingResult.MODEL_KEY_PREFIX + "signupForm");
		String errorMessages = null;
		for (ObjectError error : errors.getAllErrors()) {
			errorMessages = error.getDefaultMessage();
		}

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_70_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account70";
		String password = "Pass$#70";
		String confirmPassword = "Pass$#70";
		String name = "テスト70";
		String branchId = "1";
		String departmentId = "3"; // 組み合わせが間違っている

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "支社と部署の組み合わせが間違っています";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		String errorMessages = (String) mav.getModel().get("errorMessage");

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_71_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account71";
		String password = "Pass$#71";
		String confirmPassword = "Pass$#71";
		String name = "テスト71";
		String branchId = "1";
		String departmentId = "4"; // 組み合わせが間違っている

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "支社と部署の組み合わせが間違っています";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		String errorMessages = (String) mav.getModel().get("errorMessage");

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_72_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account72";
		String password = "Pass$#72";
		String confirmPassword = "Pass$#72";
		String name = "テスト72";
		String branchId = "2";
		String departmentId = "1"; // 組み合わせが間違っている

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "支社と部署の組み合わせが間違っています";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		String errorMessages = (String) mav.getModel().get("errorMessage");

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_73_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account73";
		String password = "Pass$#73";
		String confirmPassword = "Pass$#73";
		String name = "テスト73";
		String branchId = "2";
		String departmentId = "2"; // 組み合わせが間違っている

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "支社と部署の組み合わせが間違っています";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		String errorMessages = (String) mav.getModel().get("errorMessage");

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_74_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account74";
		String password = "Pass$#74";
		String confirmPassword = "Pass$#74";
		String name = "テスト74";
		String branchId = "3";
		String departmentId = "1"; // 組み合わせが間違っている

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "支社と部署の組み合わせが間違っています";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		String errorMessages = (String) mav.getModel().get("errorMessage");

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_75_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account75";
		String password = "Pass$#75";
		String confirmPassword = "Pass$#75";
		String name = "テスト75";
		String branchId = "3";
		String departmentId = "2"; // 組み合わせが間違っている

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "支社と部署の組み合わせが間違っています";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		String errorMessages = (String) mav.getModel().get("errorMessage");

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_76_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account76";
		String password = "Pass$#76";
		String confirmPassword = "Pass$#76";
		String name = "テスト76";
		String branchId = "4";
		String departmentId = "1"; // 組み合わせが間違っている

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "支社と部署の組み合わせが間違っています";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		String errorMessages = (String) mav.getModel().get("errorMessage");

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	@Test
	public void test_77_新規会員登録_異常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account77";
		String password = "Pass$#77";
		String confirmPassword = "Pass$#77";
		String name = "テスト77";
		String branchId = "4";
		String departmentId = "2"; // 組み合わせが間違っている

		/* URL*/
		String requestUrl = "/signup";
		String responseUrl = "signup";

		/* エラーメッセージ*/
		String errorMessage = "支社と部署の組み合わせが間違っています";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password)
				.param("confirmPassword", confirmPassword)
				.param("name", name)
				.param("branchId", branchId)
				.param("departmentId", departmentId))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		String errorMessages = (String) mav.getModel().get("errorMessage");

		assertThat(errorMessages, is(errorMessage));
		assertThat(mav.getViewName(), is(responseUrl));
	}

	private void executeScript(String file) {
		Resource resource = new ClassPathResource(file, getClass());

		ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		Connection conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
		rdp.populate(conn);

	}

}
