package jp.co.bulletinboard.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Connection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import jp.co.bulletinboard.form.LoginForm;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mvc-config.xml")
@WebAppConfiguration
@Transactional
public class LoginControllerTest {

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		executeScript("/META-INF/sql/login_test.sql");
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void test_1() throws Exception {
		LoginForm loginForm = new LoginForm();
		loginForm.setAccount("testuser");
		loginForm.setPassword("password");

		mockMvc.perform(post("/login").flashAttr("loginForm", loginForm))
				.andDo(print())
				.andExpect(view().name("redirect:/"))
				.andExpect(status().isFound());
	}

	@Test
	public void test_3() throws Exception {
		LoginForm loginForm = new LoginForm();
		loginForm.setAccount("誤ったアカウント");
		loginForm.setPassword("password");

		MvcResult mvcResult = mockMvc.perform(post("/login")
				.flashAttr("loginForm", loginForm))
				.andDo(print())
				.andExpect(view().name("login"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("loginForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.loginForm");

		String actualErrorMessage = result.getFieldError("account").getDefaultMessage();
		String expectErrorMessage = "アカウントまたはパスワードが誤っています";
		assertThat(expectErrorMessage, is(actualErrorMessage));

		String actualAccount = ((LoginForm) mav.getModel().get("loginForm")).getAccount();
		String expectAccount = "誤ったアカウント";
		assertThat(expectAccount, is(actualAccount));
	}

	@Test
	public void test_4() throws Exception {
		LoginForm loginForm = new LoginForm();
		loginForm.setAccount("testuser");
		loginForm.setPassword("誤ったパスワード");

		MvcResult mvcResult = mockMvc.perform(post("/login")
				.flashAttr("loginForm", loginForm))
				.andDo(print())
				.andExpect(view().name("login"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("loginForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.loginForm");

		String actualErrorMessage = result.getFieldError("account").getDefaultMessage();
		String expectErrorMessage = "アカウントまたはパスワードが誤っています";
		assertThat(expectErrorMessage, is(actualErrorMessage));

		String actualAccount = ((LoginForm) mav.getModel().get("loginForm")).getAccount();
		String expectAccount = "testuser";
		assertThat(expectAccount, is(actualAccount));
	}

	@Test
	public void test_5() throws Exception {
		LoginForm loginForm = new LoginForm();
		loginForm.setAccount("誤ったアカウント");
		loginForm.setPassword("誤ったパスワード");

		MvcResult mvcResult = mockMvc.perform(post("/login")
				.flashAttr("loginForm", loginForm))
				.andDo(print())
				.andExpect(view().name("login"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("loginForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.loginForm");

		String actualErrorMessage = result.getFieldError("account").getDefaultMessage();
		String expectErrorMessage = "アカウントまたはパスワードが誤っています";
		assertThat(expectErrorMessage, is(actualErrorMessage));

		String actualAccount = ((LoginForm) mav.getModel().get("loginForm")).getAccount();
		String expectAccount = "誤ったアカウント";
		assertThat(expectAccount, is(actualAccount));
	}

	@Test
	public void test_6() throws Exception {
		LoginForm loginForm = new LoginForm();
		loginForm.setAccount("stoppeduser");
		loginForm.setPassword("password");

		MvcResult mvcResult = mockMvc.perform(post("/login")
				.flashAttr("loginForm", loginForm))
				.andDo(print())
				.andExpect(view().name("login"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("loginForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.loginForm");

		String actualErrorMessage = result.getFieldError("account").getDefaultMessage();
		String expectErrorMessage = "アカウントまたはパスワードが誤っています";
		assertThat(expectErrorMessage, is(actualErrorMessage));

		String actualAccount = ((LoginForm) mav.getModel().get("loginForm")).getAccount();
		String expectAccount = "stoppeduser";
		assertThat(expectAccount, is(actualAccount));
	}

	private void executeScript(String file) {
		Resource resource = new ClassPathResource(file, getClass());

		ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		Connection conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

}
