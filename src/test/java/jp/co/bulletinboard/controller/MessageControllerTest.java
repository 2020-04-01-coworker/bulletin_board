package jp.co.bulletinboard.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Connection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mvc-config.xml")
@WebAppConfiguration
@Transactional
public class MessageControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		executeScript("/META-INF/sql/setupMessagesTable.sql");
	}

	@Test
	public void 新規投稿画面が表示出来るか() throws Exception {

		mockMvc.perform(get("/message"))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void test_31() throws Exception {
		//Controllerに投げるリクエストを作成
		MockHttpServletRequestBuilder getRequest = MockMvcRequestBuilders.post("/message")
				.param("title", "タイトル")
				.param("category", "カテゴリ")
				.param("text", "テキスト")
				.param("userId", "1");

		//Controllerにリクエストを投げる（リクエストからfinishメソッドを起動させる。）
		ResultActions results = mockMvc.perform(getRequest);

		//結果検証
		results.andDo(print());
		results.andExpect(view().name("redirect:/"));

	}

	@Test
	public void test_33_件名を31文字以上() throws Exception {
		MvcResult mvcResult = (MvcResult) mockMvc.perform(post("/message")
				.param("title", "タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル")
				.param("category", "カテゴリ")
				.param("text", "テキスト")
				.param("userId", "1"))
				.andDo(print())
				.andExpect(view().name("message"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("messageForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.messageForm");

		String actualErrorMessage = result.getFieldError("title").getDefaultMessage();
		String expectErrorMessage = "size must be between 0 and 30";
		assertThat(actualErrorMessage, is(expectErrorMessage));
		assertThat(mav.getViewName(), is("message"));

	}

	@Test
	public void test_34_本文を1001文字以上() throws Exception {
		MvcResult mvcResult = (MvcResult) mockMvc.perform(post("/message")
				.param("title", "タイトル")
				.param("category", "カテゴリ")
				.param("text",
						"テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト")
				.param("userId", "1"))
				.andDo(print())
				.andExpect(view().name("message"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("messageForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.messageForm");

		String actualErrorMessage = result.getFieldError("text").getDefaultMessage();
		String expectErrorMessage = "size must be between 0 and 1000";
		assertThat(actualErrorMessage, is(expectErrorMessage));
		assertThat(mav.getViewName(), is("message"));

	}

	@Test
	public void test_35_カテゴリを11文字以上() throws Exception {
		MvcResult mvcResult = (MvcResult) mockMvc.perform(post("/message")
				.param("title", "タイトル")
				.param("category", "カテゴリカテゴリカテゴリ")
				.param("text", "テキスト")
				.param("userId", "1"))
				.andDo(print())
				.andExpect(view().name("message"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("messageForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.messageForm");

		String actualErrorMessage = result.getFieldError("category").getDefaultMessage();
		String expectErrorMessage = "size must be between 0 and 10";
		assertThat(actualErrorMessage, is(expectErrorMessage));
		assertThat(mav.getViewName(), is("message"));

	}

	@Test
	public void test_36_件名を31文字以上かつ本文を1001文字以上かつカテゴリを11文字以上() throws Exception {
		MvcResult mvcResult = (MvcResult) mockMvc.perform(post("/message")
				.param("title", "タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル")
				.param("category", "カテゴリカテゴリカテゴリ")
				.param("text",
						"テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト")
				.param("userId", "1"))
				.andDo(print())
				.andExpect(view().name("message"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("messageForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.messageForm");

		String actualErrorMessage1 = result.getFieldError("title").getDefaultMessage();
		String expectErrorMessage1 = "size must be between 0 and 30";
		assertThat(actualErrorMessage1, is(expectErrorMessage1));
		String actualErrorMessage2 = result.getFieldError("text").getDefaultMessage();
		String expectErrorMessage2 = "size must be between 0 and 1000";
		assertThat(actualErrorMessage2, is(expectErrorMessage2));
		String actualErrorMessage3 = result.getFieldError("category").getDefaultMessage();
		String expectErrorMessage3 = "size must be between 0 and 10";
		assertThat(actualErrorMessage3, is(expectErrorMessage3));
		assertThat(mav.getViewName(), is("message"));

	}

	@Test
	public void test_37_件名を入力なし() throws Exception {
		MvcResult mvcResult = (MvcResult) mockMvc.perform(post("/message")
				.param("category", "カテゴリ")
				.param("text", "テキスト")
				.param("userId", "1"))
				.andDo(print())
				.andExpect(view().name("message"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("messageForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.messageForm");

		String actualErrorMessage = result.getFieldError("title").getDefaultMessage();
		String expectErrorMessage = "may not be empty";
		assertThat(actualErrorMessage, is(expectErrorMessage));
		assertThat(mav.getViewName(), is("message"));

	}

	@Test
	public void test_38_本文を入力なし() throws Exception {
		MvcResult mvcResult = (MvcResult) mockMvc.perform(post("/message")
				.param("title", "タイトル")
				.param("category", "カテゴリ")
				.param("userId", "1"))
				.andDo(print())
				.andExpect(view().name("message"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("messageForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.messageForm");

		String actualErrorMessage = result.getFieldError("text").getDefaultMessage();
		String expectErrorMessage = "may not be empty";
		assertThat(actualErrorMessage, is(expectErrorMessage));
		assertThat(mav.getViewName(), is("message"));

	}

	@Test
	public void test_39_カテゴリを入力なし() throws Exception {
		MvcResult mvcResult = (MvcResult) mockMvc.perform(post("/message")
				.param("title", "タイトル")
				.param("text", "テキスト")
				.param("userId", "1"))
				.andDo(print())
				.andExpect(view().name("message"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("messageForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.messageForm");

		String actualErrorMessage = result.getFieldError("category").getDefaultMessage();
		String expectErrorMessage = "may not be empty";
		assertThat(actualErrorMessage, is(expectErrorMessage));
		assertThat(mav.getViewName(), is("message"));

	}

	@Test
	public void test_40_件名かつ本文かつカテゴリにスペースのみ() throws Exception {
		MvcResult mvcResult = (MvcResult) mockMvc.perform(post("/message")
				.param("title", " ")
				.param("category", " ")
				.param("text", " ")
				.param("userId", "1"))
				.andDo(print())
				.andExpect(view().name("message"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("messageForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.messageForm");

		String actualErrorMessage1 = result.getFieldError("title").getDefaultMessage();
		String expectErrorMessage1 = "may not be empty";
		assertThat(actualErrorMessage1, is(expectErrorMessage1));
		String actualErrorMessage2 = result.getFieldError("text").getDefaultMessage();
		String expectErrorMessage2 = "may not be empty";
		assertThat(actualErrorMessage2, is(expectErrorMessage2));
		String actualErrorMessage3 = result.getFieldError("category").getDefaultMessage();
		String expectErrorMessage3 = "may not be empty";
		assertThat(actualErrorMessage3, is(expectErrorMessage3));
		assertThat(mav.getViewName(), is("message"));

	}

	@Test
	public void test_41_件名かつ本文かつカテゴリ入力なし() throws Exception {
		MvcResult mvcResult = (MvcResult) mockMvc.perform(post("/message")
				.param("userId", "1"))
				.andDo(print())
				.andExpect(view().name("message"))
				.andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("messageForm"))
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		BindingResult result = (BindingResult) mav.getModel()
				.get("org.springframework.validation.BindingResult.messageForm");

		String actualErrorMessage1 = result.getFieldError("title").getDefaultMessage();
		String expectErrorMessage1 = "may not be empty";
		assertThat(actualErrorMessage1, is(expectErrorMessage1));
		String actualErrorMessage2 = result.getFieldError("text").getDefaultMessage();
		String expectErrorMessage2 = "may not be empty";
		assertThat(actualErrorMessage2, is(expectErrorMessage2));
		String actualErrorMessage3 = result.getFieldError("category").getDefaultMessage();
		String expectErrorMessage3 = "may not be empty";
		assertThat(actualErrorMessage3, is(expectErrorMessage3));
		assertThat(mav.getViewName(), is("message"));

	}

	private void executeScript(String file) {
		Resource resource = new ClassPathResource(file, getClass());

		ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		Connection conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
		rdp.populate(conn);

	}
}
