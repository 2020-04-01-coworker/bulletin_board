package jp.co.bulletinboard.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Connection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import jp.co.bulletinboard.dto.UserDto;
import jp.co.bulletinboard.entity.User;
import jp.co.bulletinboard.filter.LoginFilter;
import jp.co.bulletinboard.filter.ManagementFilter;
import jp.co.bulletinboard.mapper.UserMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mvc-config.xml")
@WebAppConfiguration
@Transactional
public class UrlPatternTest141_146 {

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private UserMapper userMapper;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		executeScript("/META-INF/sql/urlpattern_test.sql"); //SQLの実行
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).addFilters(new LoginFilter(), new ManagementFilter()).build();
	}

	@Test
	public void test_141() throws Exception {

		UserDto dto = new UserDto();
		User entity = userMapper.getUser(1);
		BeanUtils.copyProperties(entity, dto);

		MvcResult mvcResult = mockMvc.perform(get("/setting").sessionAttr("loginUser", dto))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "./";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));
	}

	@Test
	public void test_142() throws Exception {

		UserDto dto = new UserDto();
		User entity = userMapper.getUser(1);
		BeanUtils.copyProperties(entity, dto);

		MvcResult mvcResult = mockMvc.perform(get("/signup").sessionAttr("loginUser", dto))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "./";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));
	}

	@Test
	public void test_143() throws Exception {

		UserDto dto = new UserDto();
		User entity = userMapper.getUser(1);
		BeanUtils.copyProperties(entity, dto);

		MvcResult mvcResult = mockMvc.perform(get("/setting?userId=1").sessionAttr("loginUser", dto))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "./";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));
	}

	@Test
	public void test_144() throws Exception {

		UserDto dto = new UserDto();
		User entity = userMapper.getUser(2);
		BeanUtils.copyProperties(entity, dto);

		MvcResult mvcResult = mockMvc.perform(get("/setting?userId=test").sessionAttr("loginUser", dto))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "/management";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));

		String expectErrorMessage = "不正なパラメータが入力されました";
		String actualErrorMessage = (String) mvcResult.getFlashMap().get("errorMessage");

		assertThat(expectErrorMessage, is(actualErrorMessage));
	}

	@Test
	public void test_145() throws Exception {

		UserDto dto = new UserDto();
		User entity = userMapper.getUser(2);
		BeanUtils.copyProperties(entity, dto);

		MvcResult mvcResult = mockMvc.perform(get("/setting?userId=5").sessionAttr("loginUser", dto))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "/management";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));

		String expectErrorMessage = "不正なパラメータが入力されました";
		String actualErrorMessage = (String) mvcResult.getFlashMap().get("errorMessage");

		assertThat(expectErrorMessage, is(actualErrorMessage));
	}

	@Test
	public void test_146() throws Exception {

		UserDto dto = new UserDto();
		User entity = userMapper.getUser(2);
		BeanUtils.copyProperties(entity, dto);

		MvcResult mvcResult = mockMvc.perform(get("/setting").sessionAttr("loginUser", dto))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "/management";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));

		String expectErrorMessage = "不正なパラメータが入力されました";
		String actualErrorMessage = (String) mvcResult.getFlashMap().get("errorMessage");

		assertThat(expectErrorMessage, is(actualErrorMessage));
	}


	private void executeScript(String file) {
		Resource resource = new ClassPathResource(file, getClass());

		ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		Connection conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

}
