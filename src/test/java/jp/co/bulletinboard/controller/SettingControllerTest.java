package jp.co.bulletinboard.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Connection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import jp.co.bulletinboard.form.UpdateForm;
import jp.co.bulletinboard.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mvc-config.xml")
@WebAppConfiguration
@Transactional
public class SettingControllerTest {

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void test_78_アカウント名のみ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser2")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "1")
				.param("departmentId", "1")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);
		String actualAccount = form.getAccount();
		String expectAccount = "testUser2";
		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectAccount, actualAccount);
	}

	@Test
	public void test_79_パスワードのみ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "#password123")
				.param("confirmPassword", "#password123")
				.param("name", "テスト太郎")
				.param("branchId", "1")
				.param("departmentId", "1")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);
		String actualPassword = form.getPassword();
		String expectPassword = "#password123";

		assertThat(mav.getViewName(), is("redirect:management"));
		assertTrue(passwordEncoder.matches(expectPassword, actualPassword));
	}

	@Test
	public void test_80_ユーザー名のみ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎2")
				.param("branchId", "1")
				.param("departmentId", "1")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);
		String actualName = form.getName();
		String expectName = "テスト太郎2";
		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectName, actualName);
	}

	@Test
	public void test_81_アカウント_パスワード_ユーザー名を変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser2")
				.param("password", "#password123")
				.param("confirmPassword", "#password123")
				.param("name", "テスト太郎2")
				.param("branchId", "1")
				.param("departmentId", "1")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		String actualAccount = form.getAccount();
		String expectAccount = "testUser2";

		String actualPassword = form.getPassword();
		String expectPassword = "#password123";

		String actualName = form.getName();
		String expectName = "テスト太郎2";

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectAccount, actualAccount);
		assertTrue(passwordEncoder.matches(expectPassword, actualPassword));
		assertEquals(expectName, actualName);
	}

	@Test
	public void test_82_本社_総務人事部から支社A_営業部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "2")
				.param("departmentId", "3")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 2;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 3;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_83_本社_総務人事部から支社B_営業部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "3")
				.param("departmentId", "3")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 3;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 3;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_84_本社_総務人事部から支社C_営業部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "4")
				.param("departmentId", "3")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 4;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 3;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_85_本社_総務人事部から支社A_技術部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "2")
				.param("departmentId", "4")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 2;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 4;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_86_本社_総務人事部から支社B_技術部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "3")
				.param("departmentId", "4")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 3;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 4;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_87_本社_総務人事部から支社C_技術部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart1.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "4")
				.param("departmentId", "4")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 4;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 4;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_88_本社_情報管理部から支社A_営業部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart2.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "2")
				.param("departmentId", "3")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 2;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 3;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_89_本社_情報管理部から支社B_営業部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart2.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "3")
				.param("departmentId", "3")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 3;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 3;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_90_本社_情報管理部から支社C_営業部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart2.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "4")
				.param("departmentId", "3")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 4;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 3;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_91_本社_情報管理部から支社A_技術部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart2.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "2")
				.param("departmentId", "3")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 2;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 3;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_92_本社_情報管理部から支社B_技術部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart2.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "3")
				.param("departmentId", "3")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 3;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 3;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_93_本社_情報管理部から支社C_技術部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch1Depart2.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "4")
				.param("departmentId", "3")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 4;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 3;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_94_支社A_営業部から本社_総務人事部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch2Depart3.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "1")
				.param("departmentId", "1")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 1;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 1;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_95_支社A_営業部から本社_情報管理部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch2Depart3.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "1")
				.param("departmentId", "2")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 1;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 2;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_96_支社B_営業部から本社_総務人事部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch3Depart3.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "1")
				.param("departmentId", "1")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 1;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 1;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_97_支社B_営業部から本社_情報管理部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch3Depart3.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "1")
				.param("departmentId", "2")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 1;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 2;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_98_支社C_営業部から本社_情報管理部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch4Depart3.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "1")
				.param("departmentId", "2")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 1;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 2;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	@Test
	public void test_99_支社C_営業部から本社_総務人事部へ変更() throws Exception {
		executeScript("/META-INF/sql/insertUsersBranch4Depart3.sql");

		MvcResult mvcResult = mockMvc.perform(post("/setting")
				.param("account", "testUser")
				.param("password", "password")
				.param("confirmPassword", "password")
				.param("name", "テスト太郎")
				.param("branchId", "1")
				.param("departmentId", "1")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		UpdateForm form = userService.getUser(1);

		Integer actualBranchId = form.getBranchId();
		Integer expectBranchId = 1;

		Integer actualDepartmentId = form.getDepartmentId();
		Integer expectDepartmentId = 1;

		assertThat(mav.getViewName(), is("redirect:management"));
		assertEquals(expectDepartmentId, actualDepartmentId);
		assertEquals(expectBranchId, actualBranchId);
	}

	//SQL文を実行
	private void executeScript(String file) {
		Resource resource = new ClassPathResource(file, getClass());

		ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		Connection conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}
}