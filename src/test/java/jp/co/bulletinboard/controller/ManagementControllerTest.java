package jp.co.bulletinboard.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Connection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import jp.co.bulletinboard.dto.UserBranchDepartmentDto;
import jp.co.bulletinboard.dto.UserDto;
import jp.co.bulletinboard.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mvc-config.xml")
@WebAppConfiguration
@Transactional
public class ManagementControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private UserService userService;



	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void test_42ユーザ新規登録が仕様書通り表示されるか() throws Exception {
		executeScript("/META-INF/sql/management.sql");
		mockMvc.perform(get("/signup"))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
		}

	@Test
	public void test_43_44ユーザ一編集が仕様書通り表示されるか() throws Exception {
		executeScript("/META-INF/sql/management.sql");
		UserDto userDto = new UserDto();
		userDto.setId(1);

		MockHttpSession session = new MockHttpSession();
		session.setAttribute("loginUser", userDto);

		mockMvc.perform(get("/setting")
				.param("userId","1")
				.session(session))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();
	}



	@Test
	public void test_46_47アカウント停止() throws Exception {
		executeScript("/META-INF/sql/management.sql");

		MvcResult mvcResult = mockMvc.perform(post("/management/update")
				.param("id", "1")
				.param("isStopped", "1"))

				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		List<UserBranchDepartmentDto> actuallList = userService.getUserBranchDepartmentAll();
		System.out.println("これはテストです"+ actuallList.get(0).getIsStopped());

		int expectedStopCode = 1;
		assertEquals(expectedStopCode, actuallList.get(0).getIsStopped());
		assertThat(mav.getViewName(), is("redirect:/management"));
	}

	@Test
	public void test_48_49アカウント復活() throws Exception {
		executeScript("/META-INF/sql/management.sql");
		MvcResult mvcResult = mockMvc.perform(post("/management/update")
				.param("id", "1")
				.param("isStopped", "0"))

				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		List<UserBranchDepartmentDto> actuallList = userService.getUserBranchDepartmentAll();
		System.out.println("これはテストです"+ actuallList.get(0).getIsStopped());

		int expectedRestartCode = 0;
		assertEquals(expectedRestartCode, actuallList.get(0).getIsStopped());
		assertThat(mav.getViewName(), is("redirect:/management"));
	}


	private void executeScript(String file) {
		Resource resource = new ClassPathResource(file, getClass());

		ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		Connection conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}
}
