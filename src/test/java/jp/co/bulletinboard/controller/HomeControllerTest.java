package jp.co.bulletinboard.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import jp.co.bulletinboard.dto.UserCommentDto;
import jp.co.bulletinboard.form.CommentForm;
import jp.co.bulletinboard.form.CommentForms;
import jp.co.bulletinboard.form.UserMessageForm;
import jp.co.bulletinboard.service.CommentService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mvc-config.xml")
@WebAppConfiguration
@Transactional
public class HomeControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private CommentService commentService;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void test_07_1ホーム画面のメッセージが表示出来るか() throws Exception {
		executeScript("/META-INF/sql/UsersMessageTable.sql");
		MvcResult mvcResult = mockMvc.perform(get("/"))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();

		assertTrue(mav.getModel().containsKey("allUserMessage"));
		assertTrue(mav.getModel().containsKey("userComment"));
	}

	@Test
	public void test_07_2ホーム画面のメッセージが降順で表示できるか() throws Exception {
		executeScript("/META-INF/sql/UsersMessageTable.sql");
		MvcResult mvcResult = mockMvc.perform(get("/"))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		int[] compareNum = { 5, 2, 1, 4, 3 };

		@SuppressWarnings("unchecked")
		List<UserMessageForm> actuallList = ((List<UserMessageForm>) mav.getModel().get("allUserMessage"));

		for (int i = 0; i < 5; i++) {
			assertEquals(compareNum[i], actuallList.get(i).getId());
		}
	}

	@Test
	public void test_08_2ホーム画面のコメントが昇順で表示できるか() throws Exception {
		executeScript("/META-INF/sql/UsersCommentTable.sql");
		MvcResult mvcResult = mockMvc.perform(get("/"))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		int[] compareNum = { 3, 4, 1, 2, 5 };

		@SuppressWarnings("unchecked")
		List<UserCommentDto> actuallList = ((List<UserCommentDto>) mav.getModel().get("userComment"));

		for (int i = 0; i < 5; i++) {
			assertEquals(compareNum[i], actuallList.get(i).getId());
		}

	}

	@Test
	public void test_09ログアウト後ログイン画面が表示出来るか() throws Exception {
		mockMvc.perform(get("/logout"))
				.andDo(print())
				.andExpect(status().isFound());
	}

	@Test
	public void test_10_1新規投稿画面が表示できるか() throws Exception {
		mockMvc.perform(get("/login"))
				.andDo(print())
				.andExpect(status().isOk());

		mockMvc.perform(get("/message"))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void test_18正しくコメント登録できるか() throws Exception {
		executeScript("/META-INF/sql/setupCommentsTable.sql");

		CommentForms commentForms = new CommentForms();
		CommentForm commentForm = new CommentForm();
		List<CommentForm> commentFormList = new ArrayList<>();
		commentForm.setMessageId(1);
		commentForm.setUserId(1);
		commentForm.setText("テスト");
		commentFormList.add(commentForm);
		commentForms.setCommentForm(commentFormList);

		MvcResult mvcResult = mockMvc.perform(post("/")
				.flashAttr("commentForms", commentForms)
				.param("comment", "comment"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		List<UserCommentDto> actualCommentList = commentService.getUserComment();
		int actualCommentListSize = actualCommentList.size();

		assertThat(mav.getViewName(), is("redirect:/"));
		int expectCommentsListSize = 6;
		assertEquals(expectCommentsListSize, actualCommentListSize);

	}

	@Test
	public void test_24_2投稿削除ができるか() throws Exception {
		executeScript("/META-INF/sql/UsersMessageTable.sql");

		mockMvc.perform(post("/deletemessage")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound());

		MvcResult homeMVC = mockMvc.perform(get("/"))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = homeMVC.getModelAndView();

		@SuppressWarnings("unchecked")
		List<UserMessageForm> actualList = ((List<UserMessageForm>) mav.getModel().get("allUserMessage"));
		System.out.println(StringUtils.isEmpty(actualList) + "★★");
		int actualListSize = actualList.size();
		int expectListSize = 4;
		assertEquals(expectListSize, actualListSize);
	}

	@Test
	public void test_28コメント削除ができるか() throws Exception {
		executeScript("/META-INF/sql/setupCommentsTable.sql");
		MvcResult mvcResult = mockMvc.perform(post("/deletecomment")
				.param("id", "1"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		List<UserCommentDto> actualCommentList = commentService.getUserComment();
		int actualCommentListSize = actualCommentList.size();

		assertThat(mav.getViewName(), is("redirect:/"));
		int expectCommentsListSize = 4;
		assertEquals(expectCommentsListSize, actualCommentListSize);
	}

	private void executeScript(String file) {
		Resource resource = new ClassPathResource(file, getClass());

		ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		Connection conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
		rdp.populate(conn);
	}

}
