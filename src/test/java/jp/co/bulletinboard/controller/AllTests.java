package jp.co.bulletinboard.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
         HomeControllerTest.class
        ,LoginControllerTest.class
        ,MessageControllerTest.class
        ,ManagementControllerTest.class
        ,SettingControllerTest.class
        ,SignupControllerTest.class
        ,UrlPatternTest124_129.class
        ,UrlPatternTest136_140.class
        ,UrlPatternTest141_146.class
})

public class AllTests {
}