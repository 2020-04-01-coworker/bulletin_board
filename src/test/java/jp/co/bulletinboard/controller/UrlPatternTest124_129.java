package jp.co.bulletinboard.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Connection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import jp.co.bulletinboard.dto.UserDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mvc-config.xml")
@WebAppConfiguration
@Transactional
public class UrlPatternTest124_129 {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

		executeScript("/META-INF/sql/setupUrlPatternTest.sql");
	}

	@Test
	public void test_124_URLパターン_正常系() throws Exception {
		/* URL*/
		String requestUrl = "/login";

		mockMvc.perform(get(requestUrl))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void test_125_URLパターン_正常系() throws Exception {

		/* 登録アカウント情報*/
		String account = "account";
		String password = "password";

		/* URL*/
		String requestUrl = "/login";
		String responseUrl = "redirect:/";

		MvcResult mvcResult = mockMvc.perform(post(requestUrl)
				.param("account", account)
				.param("password", password))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		mvcResult.getResponse();

		assertThat(mav.getViewName(), is(responseUrl));

	}

	@Test
	public void test_126_URLパターン_正常系() throws Exception {

		/* URL*/
		String requestUrl = "/login";

		mockMvc.perform(get(requestUrl))
				.andDo(print())
				.andExpect(status().isOk());
	}


	@Test
	public void test_127_URLパターン_正常系() throws Exception {

		/* URL*/
		String requestUrl = "/management";

		mockMvc.perform(get(requestUrl))
				.andDo(print())
				.andExpect(status().isOk());
	}


	@Test
	public void test_128_URLパターン_正常系() throws Exception {

		/* URL*/
		String requestUrl = "/management";

		mockMvc.perform(get(requestUrl))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void test_129_URLパターン_正常系() throws Exception {

		/* Param*/
		int id = 1;

		/* URL*/
//		String requestUrl = "/setting?userId=" + id;
		String requestUrl = "/setting";
		String responseUrl = "setting";

		UserDto userDto = new UserDto();
		userDto.setId(id);

		MockHttpSession session = new MockHttpSession();
		session.setAttribute("loginUser", userDto);

		MvcResult mvcResult = mockMvc.perform(get(requestUrl)
				.param("userId", Integer.toString(id))
				.session(session))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		ModelAndView mav = mvcResult.getModelAndView();
		mvcResult.getResponse();

		assertThat(mav.getViewName(), is(responseUrl));
	}

	private void executeScript(String file) {
		Resource resource = new ClassPathResource(file, getClass());

		ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
		rdp.addScript(resource);
		rdp.setSqlScriptEncoding("UTF-8");
		rdp.setIgnoreFailedDrops(true);
		rdp.setContinueOnError(false);

		Connection conn = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
		rdp.populate(conn);

	}

}
