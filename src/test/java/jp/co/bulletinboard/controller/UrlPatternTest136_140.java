package jp.co.bulletinboard.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import jp.co.bulletinboard.filter.LoginFilter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/mvc-config.xml")
@WebAppConfiguration
@Transactional
public class UrlPatternTest136_140 {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).addFilters(new LoginFilter()).build();
	}

	@Test
	public void test_136() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "login";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));
	}

	@Test
	public void test_137() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/message"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "login";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));
	}

	@Test
	public void test_138() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/setting"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "login";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));
	}

	@Test
	public void test_139() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/signup"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "login";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));
	}

	@Test
	public void test_140() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/setting"))
				.andDo(print())
				.andExpect(status().isFound())
				.andReturn();

		MockHttpServletResponse responce = mvcResult.getResponse();

		String expectUrl = "login";
		String actualUrl = responce.getRedirectedUrl();

		assertThat(expectUrl, is(actualUrl));
	}

}
