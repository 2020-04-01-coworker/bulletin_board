DELETE FROM users;

INSERT INTO users(id,account,password,name,branch_id,department_id) VALUES (1,'testUser','$2a$10$gBSYELEZkED/3dL9ZGGNSecUXeYlzk71GTql67Ys5/Xb1RTPbxGRu','テスト太郎',1,1);

--ALTER TABLE messages MODIFY id int not null; --オートインクリメントリセットするために一時的にオートインクリメントリセット
--ALTER TABLE messages AUTO_INCREMENT = 1; --オートインクリメントリセット
--ALTER TABLE messages MODIFY id int not null auto_increment; --オートインクリメント付与
--
--UPDATE users SET account = 'testUser2' ,
--		password = 'password' ,
--		name = 'テスト太郎' ,
--		branch_id = 1 ,
--		department_id = 2 ,
--		created_date = CURRENT_TIMESTAMP ,
--		updated_date = CURRENT_TIMESTAMP
--		where id = 1;
