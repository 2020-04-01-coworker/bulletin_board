DELETE FROM users;

INSERT
INTO
	users(id,account,password,name,branch_id,department_id)
VALUES (1,'testuser','$2a$10$i0FlsKe6FiEuViMhclA90uCjCCKeLhtcswz01Rwl9qTIsIY1c.ohO','ALH太郎',1,1);

INSERT
INTO
	users(id,account,password,name,branch_id,department_id,is_stopped)
VALUES (2,'stoppeduser','$2a$10$i0FlsKe6FiEuViMhclA90uCjCCKeLhtcswz01Rwl9qTIsIY1c.ohO','ALH太郎',1,1,1);
