DELETE FROM users;

ALTER TABLE users MODIFY id int not null;
ALTER TABLE users AUTO_INCREMENT = 1;
ALTER TABLE users MODIFY id int not null auto_increment;


INSERT INTO users(account,password,name,branch_id,department_id)
VALUES
('testuser','$2a$10$i0FlsKe6FiEuViMhclA90uCjCCKeLhtcswz01Rwl9qTIsIY1c.ohO','ALH三郎',1,1);

INSERT INTO users(account,password,name,branch_id,department_id)
VALUES
('testuser','$2a$10$i0FlsKe6FiEuViMhclA90uCjCCKeLhtcswz01Rwl9qTIsIY1c.ohO','ALH三郎',1,1);

INSERT INTO users(account,password,name,branch_id,department_id)
VALUES
('testuser','$2a$10$i0FlsKe6FiEuViMhclA90uCjCCKeLhtcswz01Rwl9qTIsIY1c.ohO','ALH三郎',1,1);

