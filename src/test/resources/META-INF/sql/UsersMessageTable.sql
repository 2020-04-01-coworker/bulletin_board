DELETE FROM messages;
DELETE FROM users;

INSERT INTO users(id,account,password,name,branch_id,department_id) VALUES (1,'testuser','$2a$10$i0FlsKe6FiEuViMhclA90uCjCCKeLhtcswz01Rwl9qTIsIY1c.ohO','ALH太郎',1,1);

ALTER TABLE messages MODIFY id int not null;
ALTER TABLE messages AUTO_INCREMENT = 1;
ALTER TABLE messages MODIFY id int not null auto_increment;

INSERT INTO messages(title,text,category,user_id, created_date)VALUES('タイトル','本文','カテゴリ',1,'2020-05-27 01:02:03'); --#3
INSERT INTO messages(title,text,category,user_id, created_date)VALUES('タイトル','本文','カテゴリ',1,'2020-05-28 02:03:04'); --#4
INSERT INTO messages(title,text,category,user_id, created_date)VALUES('タイトル','本文','カテゴリ',1,'2020-05-23 03:04:05'); --#1
INSERT INTO messages(title,text,category,user_id, created_date)VALUES('タイトル','本文','カテゴリ',1,'2020-05-24 04:05:06'); --#2
INSERT INTO messages(title,text,category,user_id, created_date)VALUES('タイトル','本文','カテゴリ',1,'2020-05-29 05:06:07'); --#5


