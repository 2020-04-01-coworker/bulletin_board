DELETE FROM messages;
DELETE FROM comments;

ALTER TABLE messages MODIFY id int not null;
ALTER TABLE messages AUTO_INCREMENT = 1;
ALTER TABLE messages MODIFY id int not null auto_increment;

INSERT INTO messages(title,text,category,user_id)VALUES('タイトル','本文','カテゴリ',1);

ALTER TABLE comments MODIFY id int not null;
ALTER TABLE comments AUTO_INCREMENT = 1;
ALTER TABLE comments MODIFY id int not null auto_increment;

INSERT INTO comments(text,user_id, message_id, created_date)VALUES('コメント',1,1,'2020-05-27 01:02:03'); --#3
INSERT INTO comments(text,user_id, message_id, created_date)VALUES('コメント',1,1,'2020-05-28 02:03:04'); --#4
INSERT INTO comments(text,user_id, message_id, created_date)VALUES('コメント',1,1,'2020-05-23 03:04:05'); --#1
INSERT INTO comments(text,user_id, message_id, created_date)VALUES('コメント',1,1,'2020-05-24 04:05:06'); --#2
INSERT INTO comments(text,user_id, message_id, created_date)VALUES('コメント',1,1,'2020-05-29 05:06:07'); --#5
