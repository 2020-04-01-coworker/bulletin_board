DELETE FROM users;

INSERT INTO users(id,account,password,name,branch_id,department_id)
VALUES
(1,'testuser','$2a$10$i0FlsKe6FiEuViMhclA90uCjCCKeLhtcswz01Rwl9qTIsIY1c.ohO','ALH三郎',1,1);

update users set is_stopped = 1
		where id = 1;

