DELETE FROM messages;
DELETE FROM users;

INSERT INTO users(id,account,password,name,branch_id,department_id) VALUES (1,'testuser','$2a$10$i0FlsKe6FiEuViMhclA90uCjCCKeLhtcswz01Rwl9qTIsIY1c.ohO','ALH太郎',1,1);

ALTER TABLE messages MODIFY id int not null;
ALTER TABLE messages AUTO_INCREMENT = 1;
ALTER TABLE messages MODIFY id int not null auto_increment;

INSERT INTO messages(title,text,category,user_id,created_date) VALUES ('タイトル','本文','面談者',1,'2020-04-20 01:02:03');
INSERT INTO messages(title,text,category,user_id,created_date) VALUES ('タイトル','本文','VOE面談',1,'2020-05-10 02:03:04');
INSERT INTO messages(title,text,category,user_id,created_date) VALUES ('タイトル','本文','カテゴリ',1,'2020-05-20 03:04:05');
INSERT INTO messages(title,text,category,user_id,created_date) VALUES ('タイトル','本文','カテゴリ',1,'2020-04-30 04:05:06');
INSERT INTO messages(title,text,category,user_id,created_date) VALUES ('タイトル','本文','カテゴリ',1,'2020-04-10 05:06:07');
